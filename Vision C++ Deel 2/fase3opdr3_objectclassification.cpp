#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/core/core.hpp>
#include "opencv2\ml.hpp"
#include <iostream>
#include <string>
#include <math.h>
#include <iomanip>
#include "avansvisionlib.h"
#include "fase1opdr2.h"
#include "fase2opdr3.h"
#include "fase3opdr3.h"


using namespace cv;
using namespace std;
using namespace ml;

vector<vector<double>> loadImageTrainingSet(vector<string> filenames, Mat& ITset, Mat& OTset);
double getCircularity(vector<Point> & contourVec);
void opencvImplNN();

//pi
const double PI = 3.14159265359;

// Maximale fout die toegestaan wordt in de output voor de training input
const double MAX_OUTPUT_ERROR = 1E-10;

// maximaal aantal runs dat uitgevoerd wordt bij het trainen
const int MAXRUNS = 10000;

int fase3opdr3() {
	int opencvOrJan = 1;
	if (opencvOrJan == 1) {
		opencvImplNN();
		return 0;
	}
	else {

		// IT, OT: input trainingset, output trainingset
		Mat ITset, OTset;

		// V0, W0   : weightfactor matrices
		// dV0, dW0 : weightfactor correction matrices
		Mat V0, W0, dW0, dV0;

		// default number of hiddenNeurons. The definite number is user input  
		// inputNeurons and outputNeurons are implicitly determined via
		// the trainingset, i.e.: inputNeurons = ITset.cols ; outputNeurons = OTset.cols;
		int hiddenNeurons = 2;

		cout << endl << "Load trainingset..." << endl << endl;
		vector<string> trainingSets = { "Circles", "Squares", "Triangles" };
		loadImageTrainingSet(trainingSets, ITset, OTset);
		waitKey(0);
		cout << "Training Input " << endl << endl;
		cout << ITset << endl << endl;
		cout << "Training Output " << endl << endl;
		cout << OTset << endl << endl;

		cout << " ===> BPN format: " << endl <<
			"BPN Inputlayer  = " << ITset.cols << "  neurons" << endl <<
			"BPN Outputlayer = " << OTset.cols << "  neurons" << endl << endl;
		cout << "Please choose a number of hidden neurons: ";
		cin >> hiddenNeurons;
		cout << "Thank you!" << endl << endl << endl;

		cout << "Initialize BPN ..." << endl;
		initializeBPN(ITset.cols, hiddenNeurons, OTset.cols, V0, dV0, W0, dW0);
		//testBPN(ITset, OTset, V0, dV0, W0, dW0);

		cout << "initial values of weight matrices V0 and W0" << endl;
		cout << "*******************************************" << endl;
		cout << V0 << endl << endl << W0 << endl << endl;
		cout << "Press ENTER => ";
		string dummy;
		getline(cin, dummy);
		getline(cin, dummy);

		// IT: current training input of the inputlayer 
		// OT: desired training output of the BPN
		// OH: output of the hiddenlayer
		// OO: output of the outputlayer
		Mat IT, OT, OH, OO;

		// outputError0: error on output for the current input and weighfactors V0, W0
		// outputError1: error on output for the current input and new calculated 
		//               weighfactors, i.e. V1, W1
		double outputError0, outputError1, sumSqrDiffError = MAX_OUTPUT_ERROR + 1;
		Mat V1, W1;

		int runs = 0;
		while ((sumSqrDiffError > MAX_OUTPUT_ERROR) && (runs < MAXRUNS)) {

			sumSqrDiffError = 0;

			for (int inputSetRowNr = 0; inputSetRowNr < ITset.rows; inputSetRowNr++) {

				IT = transpose(getRow(ITset, inputSetRowNr));

				OT = transpose(getRow(OTset, inputSetRowNr));

				calculateOutputHiddenLayer(IT, V0, OH);

				calculateOutputBPN(OH, W0, OO);

				adaptVW(OT, OO, OH, IT, W0, dW0, V0, dV0, W1, V1);

				calculateOutputBPNError(OO, OT, outputError0);

				calculateOutputBPNError(BPN(IT, V1, W1), OT, outputError1);

				sumSqrDiffError += (outputError1 - outputError0) * (outputError1 - outputError0);

				V0 = V1;
				W0 = W1;
			}
			cout << "sumSqrDiffError = " << sumSqrDiffError << endl;
			runs++;
		}

		cout << "BPN Training is ready!" << endl << endl;
		cout << "Runs = " << runs << endl << endl;

		Mat inputVectorTrainingSet, outputVectorTrainingSet, outputVectorBPN;

		// druk voor elke input vector uit de trainingset de output vector uit trainingset af 
		// tezamen met de output vector die het getrainde BPN (zie V0, W0) genereerd bij de 
		// betreffende input vector.
		cout << setw(16) << " " << "Training Input" << setw(12) << "|" << " Expected Output "
			<< setw(1) << "|" << " Output BPN " << setw(6) << "|" << endl << endl;
		for (int row = 0; row < ITset.rows; row++) {

			// haal volgende inputvector op uit de training set
			inputVectorTrainingSet = transpose(getRow(ITset, row));

			// druk de inputvector af in een regel afgesloten met | 
			for (int r = 0; r < inputVectorTrainingSet.rows; r++)
				cout << setw(8) << getEntry(inputVectorTrainingSet, r, 0);
			cout << setw(2) << "|";

			// haal bijbehorende outputvector op uit de training set
			outputVectorTrainingSet = transpose(getRow(OTset, row));

			// druk de outputvector van de training set af in dezelfde regel afgesloten met | 
			for (int r = 0; r < outputVectorTrainingSet.rows; r++)
				cout << setw(8) << round(getEntry(outputVectorTrainingSet, r, 0));
			cout << setw(2) << "|";

			// bepaal de outputvector die het getrainde BPN oplevert 
			// bij de inputvector uit de trainingset  
			outputVectorBPN = BPN(inputVectorTrainingSet, V0, W0);

			// druk de output vector van het BPN af in dezelfde regel afgesloten met |
			for (int r = 0; r < outputVectorBPN.rows; r++)
				cout << setw(8) << round(getEntry(outputVectorBPN, r, 0));
			cout << setw(2) << "|";

			cout << endl;
		}

		cout << endl << endl << "Press ENTER for exit";
		getline(cin, dummy);
		getline(cin, dummy);

		return 0;
	}
}

// func: loads image trainingset and extracts features
// return: features of the imageset
vector<vector<double>> loadImageTrainingSet(vector<string> filenames, Mat & ITset, Mat & OTset) {
	//vector containing features
	vector<vector<double>> features;
	//train each set
	for (int i = 0; i < filenames.size(); i++) {
		//read the image
		Mat image;
		image = imread("Images/Fase3/Trainingsset/" + filenames[i] + ".jpg", CV_LOAD_IMAGE_COLOR);

		//convert image to gray scale
		Mat gray_image;
		cvtColor(image, gray_image, CV_BGR2GRAY);

		//convert gray_image to binary
		Mat binary_image;
		threshold(gray_image, binary_image, 230, 1, CV_THRESH_BINARY_INV);
		//show the original picture
		imshow("Original image", image);
		//create contour vector
		std::vector<std::vector<cv::Point>> contourVecs;
		Mat output = binary_image.clone();
		//find all contours
		//cout << "Found " << allContours(binary_image, contourVecs) << " contours" << endl;
		cout << filenames[i] << endl;
		findContours(output, contourVecs, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		//create empty mat object with all 0's
		Mat contour_image = Mat(image.rows, image.cols, CV_16S, Scalar(0));
		//create matrix to display image with contours
		for (int j = 0; j < contourVecs.size(); j++) {
			//cout << "Bending energy: " << bendingEnergy(contourVecs[i]) << endl;
			double circularity = getCircularity(contourVecs[j]);
			double bending = bendingEnergy(contourVecs[j]);
			cout << "Bendy circularity: " << (circularity * bending) / 2 << endl;
			vector<double> temp;
			temp.push_back(circularity);
			temp.push_back(bending);
			features.push_back(temp);
			//cout <<	bendingEnergy(contourVecs[i]) << endl;
			for (int k = 0; k < contourVecs[j].size(); k++) {
				setEntryImage(contour_image, contourVecs[j][k].y, contourVecs[j][k].x, 255);
			}
		}

		//show16SImageStretch(contour_image, "contours");
	} 
	// input of trainingset (without bias)
	// remark: nummber of columns == number of inputneurons of the BPN
	ITset = Mat(features.size(),features[0].size(), CV_64F);
	for (size_t a = 0; a < features.size(); a++)
	{
		for (size_t b = 0; b < features[a].size(); b++)
		{
			ITset.at<double>(a, b) = features[a][b];
		}
	}
	// output of trainingset
	// remark: nummber of columns == number of outputneurons of the BPN
	OTset = (Mat_<double>(12, 1) <<
		0,
		0,
		0,
		0,
		1,
		1,
		1,
		1,
		2,
		2,
		2,
		2);
	return features;
}	

//requires a pointer to the contourvec and the area of the blob.
//returns the circularity on a scale of 0 to 1.
double getCircularity(vector<Point> & contourVec) {
	//determine the length of the boundary
	double perimeter = arcLength(contourVec, true);
	double area = contourArea(contourVec, false);
	//calculate circularity Rc = 4*pi*aRegion / p^2
	double circularity = (4 * PI * area) / (perimeter * perimeter);
	//return the circularity 
	return circularity;
}

void opencvImplNN() {
	// IT, OT: input trainingset, output trainingset
	Mat ITset, OTset;

	cout << endl << "Load trainingset..." << endl << endl;
	vector<string> trainingSets = { "Circles", "Squares", "Triangles" };
	loadImageTrainingSet(trainingSets, ITset, OTset);
	//loadTrainingSet1(ITset, OTset);
	//loadBinaryTrainingSet1(ITset, OTset);

	//define layer sizes
	int inputLayerSize = ITset.cols;
	int hiddenLayerSize = 50;
	int outputLayerSize = OTset.cols;
	int numSamples = 12;

	Mat layerSizes = Mat(3, 1, CV_16U);
	layerSizes.row(0) = Scalar(inputLayerSize);
	//defining hidden layer is not obligated
	layerSizes.row(1) = Scalar(hiddenLayerSize);
	layerSizes.row(2) = Scalar(outputLayerSize);

	//create the network
	cout << "neural network succesfully created." << endl;
	Ptr<ANN_MLP> nnPtr = ANN_MLP::create();

	//set the sizes of the layers this needs to be called before the activation function for some reason. otherwise it may cause errors as of opencv 3.1
	nnPtr->setLayerSizes(layerSizes);

	//choose an activation function, I chose sigmoid, because its the only one in opencv at the moment
	nnPtr->setActivationFunction(ANN_MLP::SIGMOID_SYM);

	//set training method
	nnPtr->setTrainMethod(ANN_MLP::BACKPROP);
	nnPtr->setBackpropMomentumScale(0.1);
	nnPtr->setBackpropWeightScale(0.1);
	nnPtr->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, (int)1000000, 1e-6));

	cout << "neural network succesfully configured." << endl;

	//output the samples and the responses
	cout << "samples:\n" << ITset << endl;
	cout << "\nresponses:\n" << OTset << endl;

	//convert the mat objects to make sure they are suitable for training the nn
	ITset.convertTo(ITset, CV_32F);
	OTset.convertTo(OTset, CV_32F);

	//train the network
	if (nnPtr->train(ITset, ml::ROW_SAMPLE, OTset)) {
		cout << "neural network succesfully trained." << endl;

		//display a calculated weight to see if it makes "sense"
		cout << "\nweights[0]:\n" << nnPtr->getWeights(0) << endl;

		waitKey(0);
		//predict the samples
		Mat output;
		nnPtr->predict(ITset, output);
		cout << "\noutput:\n" << output << endl;
	}
	else {
		cout << "training failed" << endl;
	}
	waitKey(0);
	
}