#pragma once
void fase2opdr2();
int allBoundingBoxes(const vector<vector<Point>> & contours, vector<vector<Point>> & bbs);
void segregateImages(Mat& image, string& filename, vector<vector<Point>>& objectPixels);
void getExtremes(vector<vector<Point>>& scanVector, vector<Point>& extremeValuesLow, vector<Point>& extremeValuesHigh, vector<Point>& dimensions);