#pragma once
void fase2opdr3();
int enclosedPixels(const vector<Point> & contourVec, vector<Point> & regionPixels);
int waterproofContour(vector<Point> & contourVec, vector<Point> & newContourVec);
void segregateImagesEnclosed(Mat& image, string& filename, vector<vector<Point>>& objectPixels);
bool inVector(const vector<Point> & contourVec, Point point);