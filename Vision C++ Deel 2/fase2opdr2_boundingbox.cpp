#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "avansvisionlib.h"
#include <math.h>
#include "fase1opdr2.h"

using namespace cv;
using namespace std;

int allBoundingBoxes(const vector<vector<Point>> & contours, vector<vector<Point>> & bbs);
void segregateImages(Mat& image, string& filename, vector<vector<Point>>& objectPixels);
void getExtremes(vector<vector<Point>>& scanVector, vector<Point>& extremeValuesLow, vector<Point>& extremeValuesHigh, vector<Point>& dimensions);

void fase2opdr2() {
	//read the image with the filename

	string filename = "dogs";

	Mat image;
	image = imread("Images/Fase2/" + filename + ".jpg", CV_LOAD_IMAGE_COLOR);

	//convert image to gray scale
	Mat gray_image;
	cvtColor(image, gray_image, CV_BGR2GRAY);

	//convert image to binary
	Mat binary_image;
	threshold(gray_image, binary_image, 240, 1, CV_THRESH_BINARY_INV);
	//show the original picture
	imshow("Original image", image);
	//create contour vector
	vector<vector<Point>> contourVecs;
	//find all contours
	cout << "Found " << allContours(binary_image, contourVecs) << " contours" << endl;
	//create empty mat object with all 0's
	Mat contour_image = Mat(binary_image.rows, binary_image.cols, CV_16S, Scalar(0));
	//create matrix to display image with contours
	for (int i = 0; i < contourVecs.size(); i++) {
		for (int j = 0; j < contourVecs[i].size(); j++) {
			setEntryImage(contour_image, contourVecs[i][j].x, contourVecs[i][j].y, 255);
		}
	}
	//find the bounding boxes
	vector<vector<Point>> boundingBoxes;
	allBoundingBoxes(contourVecs, boundingBoxes);
	//draw bounding boxes with contours
	for (int k = 0; k < boundingBoxes.size(); k++) {
		for (int l = 0; l < boundingBoxes[k].size(); l++) {
			setEntryImage(contour_image, boundingBoxes[k][l].x, boundingBoxes[k][l].y, 255);
		}
	}
	//show image with contours and bounding box
	show16SImageStretch(contour_image, "Bounding box");

	segregateImages(image, filename, boundingBoxes);
	waitKey(0);
}

// func: delivers bounding Boxes of contours
// pre: contours contains the contours for which bounding boxes have to be delivered
// post: bbs contains all bounding boxes. The index corresponds to the index of contours.
// I.e. bbs[i] belongs to contours[i]
int allBoundingBoxes(const vector<vector<Point>> & contours, vector<vector<Point>> & bbs) {
	//loop trough all contours
	vector<Point> boundingBox;
	for (int i = 0; i < contours.size(); i++) {
		//define 2 points
		Point2d leftTopCorner = contours[i][0];
		Point2d rightBottomCorner = contours[i][0];
		//clear boundingbox
		boundingBox.clear();
		for (int j = 0; j < contours[i].size(); j++) {
			//get the extreme values of the contour
			if (leftTopCorner.x > contours[i][j].x) { leftTopCorner.x = contours[i][j].x; }
			if (leftTopCorner.y < contours[i][j].y) { leftTopCorner.y = contours[i][j].y; }
			if (rightBottomCorner.x < contours[i][j].x) { rightBottomCorner.x = contours[i][j].x; }
			if (rightBottomCorner.y > contours[i][j].y) { rightBottomCorner.y = contours[i][j].y; }
		}
		//draw a line between the points, start drawing on the lefttopcorner, add each point to the vector.
		//first calculate the height and width of the box;
		int height = leftTopCorner.y - rightBottomCorner.y;
		int width = rightBottomCorner.x - leftTopCorner.x;
		//use a loop to fill the boundingbox vector vertically
		for (int k = 0; k < height; k++) {
			//create 2 points
			Point2d left, right;
			//assign coordinates to the points
			left.x = leftTopCorner.x;
			right.x = rightBottomCorner.x;
			left.y = rightBottomCorner.y + k;
			right.y = rightBottomCorner.y + k;

			//add them to the boundingbox vector.
			boundingBox.push_back(left);
			boundingBox.push_back(right);
		}
		//use a loop to fill the boundingbox vector horizontally
		for (int l = 0; l < width; l++) {
			//create 2 points
			Point2d top, bottom;
			//assign coordinates to the points
			top.y = leftTopCorner.y;
			bottom.y = rightBottomCorner.y;
			top.x = leftTopCorner.x + l;
			bottom.x = leftTopCorner.x + l;
			//add them to the boundingbox vector.
			boundingBox.push_back(top);
			boundingBox.push_back(bottom);
		}
		//vector is full, add to vector with boxes.
		bbs.push_back(boundingBox);
	}
	return bbs.size();
}

void segregateImages(Mat& image, string& filename, vector<vector<Point>>& objectPixels) {
	//create a vector to store the segregated images.
	vector<Mat> segregatedImages;
	//determine the width and height of the biggest box
	int width = 0, height = 0;
	//get the extreme values
	vector<Point> extremeValuesLow;
	vector<Point> extremeValuesHigh;
	vector<Point> dimensions;
	//get the extreme values
	getExtremes(objectPixels, extremeValuesLow, extremeValuesHigh, dimensions);
	//loop trough the dimensions to find the max width and height
	for (int i = 0; i < dimensions.size(); i++) {
		if (dimensions[i].x > width) { width = dimensions[i].x; }
		if (dimensions[i].y > height) { height = dimensions[i].y; }
	}
	//now loop trough the boxes in order to create the images
	for (int i = 0; i < objectPixels.size(); i++) {
		//create an empty image with the right size.
		Mat background(Mat(width, height, CV_8UC3));
		background = Scalar(255, 255, 255);
		//segregate the image from the original image
		//create a rectangle to cut out
		Rect roi(extremeValuesLow[i].y, extremeValuesLow[i].x, dimensions[i].y, dimensions[i].x);
		//check for ROI errors
		if (roi.x >= 0 && roi.y >= 0 && roi.width + roi.x < image.cols && roi.height + roi.y < image.rows) {
			//copy the roi over the background
			Mat roiImg;
			roiImg = image(roi);
			roiImg.copyTo(background(
				Rect((height - dimensions[i].y) / 2, (width - dimensions[i].x) / 2, dimensions[i].y, dimensions[i].x)));
		}
		segregatedImages.push_back(background);
	}
	//save the images, for each item in vector, save image
	for (int z = 0; z < segregatedImages.size(); z++) {
		imwrite("Images/Fase2/segregatedImages/" + filename + "_" + to_string(z) + ".bmp", segregatedImages[z]);
	}
}

// func: delivers extreme values of a vector series of vectors
void getExtremes(vector<vector<Point>>& scanVector, vector<Point>& extremeValuesLow, vector<Point>& extremeValuesHigh, vector<Point>& dimensions) {
	//loop trough vectors
	for (int i = 0; i < scanVector.size(); i++) {
		vector<int> xValues, yValues;

		//store all the x values and y values in seperate arrays
		for (int j = 0; j < scanVector[i].size(); j++) {
			xValues.push_back(scanVector[i][j].x);
			yValues.push_back(scanVector[i][j].y);
		}

		Point2d extremeLow, extremeHigh, dimension;

		//calculate values
		extremeLow.x = *min_element(xValues.begin(), xValues.end());
		extremeLow.y = *min_element(yValues.begin(), yValues.end());
		extremeHigh.x = *max_element(xValues.begin(), xValues.end());
		extremeHigh.y = *max_element(yValues.begin(), yValues.end());
		dimension.x = extremeHigh.x - extremeLow.x;
		dimension.y = extremeHigh.y - extremeLow.y;
		
		//push values into array
		extremeValuesLow.push_back(extremeLow);
		extremeValuesHigh.push_back(extremeHigh);
		dimensions.push_back(dimension);
	}
}