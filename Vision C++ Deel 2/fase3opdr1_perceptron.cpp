#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "avansvisionlib.h"
#include <math.h>
#include "fase1opdr2.h"
#include "fase2opdr2.h"
#include <iomanip>

using namespace cv;
using namespace std;

double perceptronOutput(double x1, double x2, double x3, double W1, double W2, double W3, double threshold);

void fase3opdr1() {
	//// waarheidstabel AND functie
	//double tabel[4][3] = { 
	//{ 0.0, 0.0, 0.0 },
	//{ 0.0, 1.0, 0.0 },
	//{ 1.0, 0.0, 0.0 },
	//{ 1.0, 1.0, 1.0 } };

	//waarheidstabel x1 and x2 or x3 functie
	double tabel1[6][4] = {
		{ 0.0, 0.0, 0.0 , 0.0},
		{ 1.0, 0.0, 0.0 , 0.0 },
		{ 0.0, 1.0, 0.0 , 0.0 },
		{ 1.0, 1.0, 0.0 , 1.0 },
		{ 1.0, 1.0, 1.0 , 1.0 },
		{ 0.0, 0.0, 1.0 , 1.0 },
		
 };

	// startwaarden van de weegfactoren (willekeurig)
	double W1 = 1003;
	double W2 = 456;
	//extra weegfactor toevoegen
	double W3 = 48;

	// startwaarde van de threshold (willekeurig)
	double threshold = 185;

	// input en output variabele
	double input, output, deltaOutput;

	// aanpassingen
	double deltaThreshold = 0.0, deltaW1 = 0.0, deltaW2 = 0.0, deltaW3 = 0.0;

	// boolean flag wordt gehezen als output meer dan EPSILON afwijkt van verwachte output
	bool flag = false;
	const int EPSILON = 0.000001;

	// waarheidstabel afdrukken 
	cout << "Waarheidstabel waarvoor het perceptron wordt getraind" << endl;
	for (int rij = 0; rij < 6; rij++) {
		cout << setw(4) << tabel1[rij][0] << setw(4) << tabel1[rij][1] << setw(4) << tabel1[rij][2] << setw(4)<< tabel1[rij][3] << endl;
		cout << endl;
	}
	cout << "Press ENTER to continue" << endl;
	cin.ignore();

	int aantalRuns = 0;
	do {
		// voer een run uit
		flag = false;
		for (int rij = 0; rij < 6; rij++) {

			// bereken actuele output van het nog ongetrainde perceptron
			output = perceptronOutput(tabel1[rij][0], tabel1[rij][1], tabel1[rij][2], W1, W2, W3, threshold);

			// bereken de afwijking van de actuele output t.o.v. verwachte output
			deltaOutput = (output - tabel1[rij][3]);

			if (abs(deltaOutput) > EPSILON) {

				flag = true;

				// berekening aanpassing W1 en W2
				deltaW1 = -deltaOutput*tabel1[rij][0];
				deltaW2 = -deltaOutput*tabel1[rij][1];
				deltaW3 = -deltaOutput*tabel1[rij][2];

				// threshold, W1 en W2 aanpassen
				threshold += deltaOutput;
				W1 += deltaW1;
				W2 += deltaW2;
				W3 += deltaW3;

			} // if

		} // for

		cout << "Runs " << ++aantalRuns <<
			"   Threshold = " << threshold << endl;
	} while (flag);

	cout << "Berekende threshold = " << threshold << endl;
	cout << "Berekende W1 = " << W1 << endl;
	cout << "Berekende W2 = " << W2 << endl;
	cout << "Berekende W3 = " << W3 << endl;
	cout << "Press ENTER to continue" << endl;
	cin.ignore();

	// genereer waarheidstabel met getrainde perceptron
	cout << "waarheidstabel gegenereerd met perceptron => " << endl;
	for (int rij = 0; rij < 6; rij++) {
		cout << setw(4) << tabel1[rij][0] << setw(4) << tabel1[rij][1] << setw(4) << tabel1[rij][2] << setw(4)
			<< perceptronOutput(tabel1[rij][0], tabel1[rij][1], tabel1[rij][2], W1, W2, W3, threshold) << endl;
	}

	cout << "Press ENTER to continue" << endl;
	cin.ignore();

}

double perceptronOutput(double x1, double x2, double x3, double W1, double W2, double w3, double threshold) {
	// bereken input
	double input = W1*x1 + W2*x2 + w3*x3;

	// bepaal output
	if (input > threshold) return 1.0;
	else return 0.0;
} // perceptronOutput
