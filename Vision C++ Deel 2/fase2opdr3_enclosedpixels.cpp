#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "avansvisionlib.h"
#include <math.h>
#include "fase1opdr2.h"
#include "fase2opdr2.h"

using namespace cv;
using namespace std;

int enclosedPixels(const vector<Point> & contourVec, vector<Point> & regionPixels);
int waterproofContour(vector<Point> & contourVec , vector<Point> & newContourVec);
void segregateImagesEnclosed(Mat& image, string& filename, vector<vector<Point>>& objectPixels);
bool inVector(const vector<Point> & contourVec, Point point);
bool createLargerContour(Mat & image, const vector<Point> & contourVec, int steps);

void fase2opdr3() {
	//read the image
	Mat image;
	string filename = "leaves2";
	image = imread("Images/Fase2/" + filename + ".jpg", CV_LOAD_IMAGE_COLOR);

	//convert image to gray scale
	Mat gray_image;
	cvtColor(image, gray_image, CV_BGR2GRAY);

	//convert image to binary
	Mat binary_image;
	threshold(gray_image, binary_image, 230, 1, CV_THRESH_BINARY_INV);
	//show the original picture
	imshow("Original image", image);
	//create contour vector
	vector<vector<Point>> contourVecs;
	//find all contours
	cout << "Found " << allContours(binary_image, contourVecs) << " contours" << endl;

	//create empty mat object with all 0's
	Mat contour_image = Mat(binary_image.rows, binary_image.cols, CV_16S, Scalar(0));
	vector<vector<Point>> newContours;
	//create matrix to display image with contours
	for (int i = 0; i < contourVecs.size(); i++) {
		vector<Point> newContour;
		cout << "Added pixels: " << waterproofContour(contourVecs[i], newContour) << endl;
		newContours.push_back(newContour);
		for (int j = 0; j < contourVecs[i].size(); j++) {
			setEntryImage(contour_image, contourVecs[i][j].x, contourVecs[i][j].y, 255);
		}
	}

	//enclosedpixels
	vector<vector<Point>> regionPixels;
	for (int i = 0; i < contourVecs.size(); i++) {
		vector<Point> regionPixel;
		cout << "Found pixels: " << enclosedPixels(newContours[i], regionPixel) << endl;
		regionPixels.push_back(regionPixel);
		regionPixel.clear();
	}

	//show image with contours and region pixels 
	Mat flood_image = Mat(binary_image.rows, binary_image.cols, CV_16S, Scalar(0));
	for (int k = 0; k < regionPixels.size(); k++) {
		for (int l = 0; l < regionPixels[k].size(); l++) {
			setEntryImage(flood_image, regionPixels[k][l].x, regionPixels[k][l].y, 255);
		}
	}
	show16SImageStretch(contour_image, "Contours");
	show16SImageStretch(flood_image, "Flood fill");
	segregateImagesEnclosed(image, filename, regionPixels);

	waitKey(0);
}

// func: finds all coordinates enclosed by a contour
// pre: contourVec contains the points of a contour (=boundary).
// post: regionPixels contains all the enclosedPixels inclusive the contourpixels
// return_value: the total number of pixels (the total area).
// evaluation: this function uses the Boundary Fill or Flood Fill algorithm
int enclosedPixels(const vector<Point> & contourVec, vector<Point> & regionPixels) {
	//find initial position we will look to the bottom right (1,1) from the first pixel check to make sure it isnt part of the contour
	Point initialPosition = Point(contourVec[0].x+1, contourVec[0].y+1);
	if (inVector(contourVec, initialPosition)) {
		return 0;
	}
	//setup matrix to use for flooding
	Mat flood = Mat(1024,1024, CV_16S, Scalar(-1));
	for (int i = 0; i < contourVec.size(); i++) {
		//we set the entries of the boundary to 100 in the matrix
		setEntryImage(flood, contourVec[i].x, contourVec[i].y, 100);
		//add the boundary to the regionPixels too
		regionPixels.push_back(contourVec[i]);
	}
	//create bigger boundaries for thin lines (optional)
	createLargerContour(flood, contourVec, 3);
	//these are the 4 positions surrounding a pixel where we want to flood (4-connected)
	vector<Point> neighbours = { Point(-1,0), Point(0,-1), Point(1,0),Point(0,1) };
	//add the initial position to regionpixels
	regionPixels.push_back(initialPosition);
	//we have create a vector containing all the points that need to be searched
	vector<Point> search;
	search.push_back(initialPosition);
	//keep track of position
	int pos = 0;
	//while we have entries in the search stack keep going
	while (search.size() > 0) {
		//we search all 4 positions
		for (int j = 0; j < 4; j++)
		{
			Point searchPosition = Point(search[0].x + neighbours[j].x, search[0].y + neighbours[j].y);
			if (searchPosition.x < 0 || searchPosition.y < 0 || searchPosition.x > 1024 || searchPosition.y > 1024) {
				show16SImageStretch(flood, "error");
			}
			//check if the position collides with the contour or has already been searched before if below 0 there is no boundary or existing regionpixel
			if (getEntryImage(flood, searchPosition.x, searchPosition.y) < 0) {
				setEntryImage(flood, searchPosition.x, searchPosition.y, pos);
				//found point in region add to regionPixels and to search stack
				regionPixels.push_back(searchPosition);
				search.push_back(searchPosition);
				//increase position so we can trace back to the start position
				pos++;
			}
		}
		search.erase(search.begin());
	}
	return regionPixels.size();
}

// func: loops through point vector to find a point element (could maybe use optimization)
bool inVector(const vector<Point> & contourVec, Point point) {
	for (int i = 0; i < contourVec.size(); i++) {
		if (contourVec[i] == point) {
			return true;
		}
	}
	return false;
}

// func: waterproof an 8-connected contour
// pre: contourVec contains the points of a contour (=boundary).
// post: newContourVec contains a waterproofed contour
// return_value: total number of pixels added as a result of waterproofing
int waterproofContour(vector<Point> & contourVec, vector<Point> & newContourVec) {
	int addedPixels = 0;
	vector<Point> newcontour;
	for (int i = 0; i < contourVec.size()-1; i++) {
		newcontour.push_back(contourVec[i]);
		//if x and y are smaller we are going right and up
		if ((contourVec[i].x < contourVec[i + 1].x) && (contourVec[i].y < contourVec[i+1].y)) {
			//fill pixel above (x + 1)
			newcontour.push_back(Point(contourVec[i].x + 1, contourVec[i].y));
			addedPixels++;
		}
		//if x and y are smaller we are going left and down
		if ((contourVec[i].x > contourVec[i + 1].x) && (contourVec[i].y > contourVec[i + 1].y)) {
			//fill pixel below (x - 1)
			newcontour.push_back(Point(contourVec[i].x - 1, contourVec[i].y));
			addedPixels++;
		}
		//if x is bigger and y is smaller we are going left and up
		if ((contourVec[i].x > contourVec[i + 1].x) && (contourVec[i].y < contourVec[i + 1].y)) {
			//fill pixel to the right (y + 1)
			newcontour.push_back(Point(contourVec[i].x, contourVec[i].y + 1));
			addedPixels++;
		}
		//if x is smaller and y is bigger we are going right and down
		if ((contourVec[i].x < contourVec[i + 1].x) && (contourVec[i].y > contourVec[i + 1].y)) {
			//fill pixel to the left (y - 1)
			newcontour.push_back(Point(contourVec[i].x, contourVec[i].y - 1));
			addedPixels++;
		}
	}

	newcontour.push_back(contourVec[contourVec.size()-1]);
	newContourVec = newcontour;
	return addedPixels;
}

void segregateImagesEnclosed(Mat& image, string& filename, vector<vector<Point>>& objectPixels) {
	//create a vector to store the segregated images.
	vector<Mat> segregatedImages;
	Mat maskedImage;
	//determine the width and height of the biggest box
	int width = 0, height = 0;
	//get the extreme values
	vector<Point> extremeValuesLow;
	vector<Point> extremeValuesHigh;
	vector<Point> dimensions;
	//get the extreme values
	getExtremes(objectPixels, extremeValuesLow, extremeValuesHigh, dimensions);
	//loop trough the dimensions to find the max width and height
	for (int i = 0; i < dimensions.size(); i++) {
		if (dimensions[i].x > width) { width = dimensions[i].x; }
		if (dimensions[i].y > height) { height = dimensions[i].y; }
	}
	//now loop trough the boxes in order to create the images
	for (int i = 0; i < objectPixels.size(); i++) {
		//create an empty image with the right size.
		Mat background(Mat(width, height, CV_8UC3));
		background = Scalar(255, 255, 255);
		//create a copy in order to not overwrite the original image
		Mat imagecopy;
		Mat mask;
		image.convertTo(imagecopy, CV_8UC3);
		//create a 16S mask to fill in
		mask = Mat(imagecopy.rows, imagecopy.cols, CV_16S);
		mask = Scalar(255, 255, 255);
		//set all pixels in contour to 255
		for (int l = 0; l < objectPixels[i].size(); l++) {
			setEntryImage(mask, objectPixels[i][l].x, objectPixels[i][l].y, 1);
		}
		//after filling the 16S mask we want to convert it back to 3 channels
		Mat newMask;
		Mat channels[] = { mask, mask, mask };
		merge(channels, 3, newMask);
		newMask.convertTo(newMask, CV_8UC3);
		//create a masked image and copy it
		maskedImage = imagecopy.mul(newMask);
		//segregate the image from the original image
		//create a rectangle to cut out
		Rect roi(extremeValuesLow[i].y, extremeValuesLow[i].x, dimensions[i].y, dimensions[i].x);
		//check for ROI errors
		if (roi.x >= 0 && roi.y >= 0 && roi.width + roi.x < image.cols && roi.height + roi.y < image.rows) {
			//copy the roi over the background
			Mat roiImg;
			roiImg = maskedImage(roi);
			roiImg.copyTo(background(Rect((height - dimensions[i].y) / 2, (width - dimensions[i].x) / 2, dimensions[i].y, dimensions[i].x)));
		}
		segregatedImages.push_back(background);
	}
	//save the images, for each item in vector, save image
	for (int z = 0; z < segregatedImages.size(); z++) {
		imwrite("Images/Fase2/segregatedImagesEnclosed/" + filename + "_" + to_string(z) + ".bmp", segregatedImages[z]);
	}
}

bool createLargerContour(Mat & image, const vector<Point> & contourVec, int steps) {
	//we need a local variable because the original contourVec is constant and should not change
	vector<Point> contourVecOuter = contourVec;
	//the first pixel to search at, we want to start left of the original contour to find the new boundary
	Point2d* firstPixel = new Point2d(contourVecOuter[0].x - 1, contourVecOuter[0].y);
	//we loop for the amount provided by steps
	for (int i = 0; i < steps; i++) {
		if (findContour(image, contourVecOuter, firstPixel)) {
			//if contour is found reset image so we can add another boundary
			image = Scalar(-1);
			//waterproof it preventing "leaks"
			waterproofContour(contourVecOuter, contourVecOuter);
			//fill the image with the new contour
			for (int k = 0; k < contourVecOuter.size(); k++) {
				setEntryImage(image, contourVecOuter[k].x, contourVecOuter[k].y, 300);
			}
		}
		else {
			return false;
		}
		//set firstpixel to the left again
		firstPixel = new Point2d(contourVecOuter[0].x - 1, contourVecOuter[0].y);
		//we want to clear the vector if we are NOT at the last step to start over 
		if (i != steps - 1) { contourVecOuter.clear(); }
	}
	return true;
}