#pragma once
void fase1opdr2();

int allContours(Mat binaryImage, vector<vector<Point>> &contourVecVec);
bool findContour(Mat inputImage, vector<Point> &contourVec, Point2d* firstPixel);
double bendingEnergy(const vector<Point> & contourVec);
vector<double> getContoursAreaSize(Mat binaryImage);