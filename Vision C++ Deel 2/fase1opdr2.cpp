#include "opencv2/imgproc/imgproc.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "avansvisionlib.h"
#include <math.h>

using namespace cv;
using namespace std;

int allContours(Mat binaryImage, vector<vector<Point>> &contourVecVec);
bool findContour(Mat inputImage, vector<Point> &contourVec, Point2d* firstPixel);
bool findNeighbour(Mat inputImage, int position, Point2d firstPxl, Point2d foundPixel, vector<Point> &contourVec);
double bendingEnergy(const vector<Point> & contourVec);
vector<int> getChaincode(const vector<Point>& contourVec);
vector<int> normalizeChaincode(vector<int>& chaincode);
vector<double> getContoursAreaSize(Mat binaryImage);
vector<double> areaSize;

void fase1opdr2() {
	//read the image
	Mat image;
	image = imread("Images/Opdracht2/dog.jpg", CV_LOAD_IMAGE_COLOR);

	//convert image to gray scale
	Mat gray_image;
	cvtColor(image, gray_image, CV_BGR2GRAY);

	//convert image to binary
	Mat binary_image;
	threshold(gray_image, binary_image, 150, 1, CV_THRESH_BINARY_INV);
	//show the original picture
	imshow("Original image", image);
	//create contour vector
	vector<vector<Point>> contourVecs;
	//find all contours
	cout << "Found " << allContours(binary_image, contourVecs) << " contours" << endl;
	//create empty mat object with all 0's
	Mat contour_image = Mat(binary_image.rows, binary_image.cols, CV_16S, Scalar(0));
	//create matrix to display image
	for (int i = 0; i < contourVecs.size(); i++) {
		//display bending energy here
		cout << "bendingEnergy: " << bendingEnergy(contourVecs[i]) << endl;
		//start drawing contours here
		for (int j = 0; j < contourVecs[i].size(); j++) {
			setEntryImage(contour_image, contourVecs[i][j].x, contourVecs[i][j].y, 255);
		}
	}
	//show image with contours
	show16SImageStretch(contour_image, "Contours");
	waitKey(0);
}

// func: find all contours of all blobs in a binary image
// pre : binaryImage has depth 16 bits signed int. Contains only values 0 and 1.
// post: contourVecVec: contains the points of the contour of each blob.
// return_value: the total number of objects.
int allContours(Mat binaryImage, vector<vector<Point>> &contourVecVec) {
	//Convert binary image to 16 signed
	Mat binary_image_16S;
	binaryImage.convertTo(binary_image_16S, CV_16S);

	//Label BLOBs to find first pixels
	Mat labeled_binary_image;
	vector<Point2d*> firstpixelVec;
	vector<Point2d*> posVec;
	vector<int> areaVec;
	labelBLOBsInfo(binary_image_16S, labeled_binary_image, firstpixelVec, posVec, areaVec, 20, INT_MAX);

	for (int j = 0; j < areaVec.size(); j++) {
		areaSize.push_back(areaVec[j]);
	}
	vector<Point> contour_image;

	for (int i = 0; i < firstpixelVec.size(); i++) {
		//For each first pixel found, find the contour
		if (findContour(labeled_binary_image, contour_image, firstpixelVec[i])) {
			//if found add to contourVecVec
			contourVecVec.push_back(contour_image);
		}
		//empty image to start searching the next one
		contour_image.clear();
	}

	return contourVecVec.size();
}

vector<double> getContoursAreaSize(Mat binaryImage) {
	//Convert binary image to 16 signed
	Mat binary_image_16S;
	binaryImage.convertTo(binary_image_16S, CV_16S);
	show16SImageStretch(binary_image_16S);
	//Label BLOBs to find first pixels
	Mat labeled_binary_image;
	vector<Point2d*> firstpixelVec2;
	vector<Point2d*> posVec2;
	vector<int> areaVec2;
	labelBLOBsInfo(binary_image_16S, labeled_binary_image, firstpixelVec2, posVec2, areaVec2, 1, INT_MAX);

	for (int j = 0; j < areaVec2.size(); j++) {
		areaSize.push_back(areaVec2[j]);
	}
	return areaSize;
}

// func: finds all contours of all blobs in an image. setup variables to start searching.
// pre : inputImage has depth 16 bits signed int. Contains only values 0 and 1.
// post: contourVec: contains the points of the contour for current blob.
bool findContour(Mat inputImage, vector<Point> &contourVec, Point2d* firstPixel) {
	//first pixel found, we remember this to make sure we stop if we come accross it
	Point2d firstPxl = Point2d(firstPixel->x, firstPixel->y);
	//found pixel, we start looking at neighbours from here
	Point2d foundPixel = firstPxl;
	//add first found pixel to contourVec
	contourVec.push_back(foundPixel);
	//we start looking at position 0 which is left of the current found pixel
	int position = 0;
	//call findNeighbour to find the first neighbour
	//if it returns true there was a complete contour found 
	if (findNeighbour(inputImage, 0, firstPxl, foundPixel, contourVec)) {
		return true;
	}
}

// func: finds neighbour of current pixel found
// pre : inputImage has depth 16 bits signed int. Contains only values 0 and 1.
// pre : position to start search neighbour pixel
// pre : firstPixel contains the first pixel found
// pre : foundPixel contains the pixel last found
// post: contourVec: contains the points of the contour for current blob.
bool findNeighbour(Mat inputImage, int position, Point2d firstPxl, Point2d foundPixel, vector<Point> &contourVec) {
	//we are using the following lookup table starting at the left going clockwise
	//-- 1 -- 2 -- 3 --
	//-- 0 -- x -- 4 --
	//-- 7 -- 6 -- 5 --
	//list of points that are possible neighbours as described above we use point2d to store these
	vector<Point2d> neighbours = { Point2d(-1,0), Point2d(-1,-1), Point2d(0,-1), Point2d(1,-1), Point2d(1,0),Point2d(1,1),Point2d(0,1), Point2d(-1,1) };
	//we have to look up all 8 possible neighbour positions
	for (int i = 0; i < 8; i++) {
		//looking at 8 positions so after 7 go back to 0
		position = position % 8;
		//prevent negative numbers because we are subtracting positions
		if (position < 0) { position = 8 + position; }
		//if foundpixel neighbour at the current position equals firstpxl we have found everything
		if ((foundPixel.x + neighbours[position].x) == firstPxl.x && (foundPixel.y + neighbours[position].y) == firstPxl.y) {
			return true;
		}
		//look for a neighbour which contains a value higher then 0
		//get the x and y value for the foundpixel and add the neighbour values depending on which position we are looking at
		if (getEntryImage(inputImage, (foundPixel.x + neighbours[position].x), (foundPixel.y + neighbours[position].y)) > 0) {
			//if true we add the neighbour position we found to the contour
			//save the current position as the next found pixel 
			foundPixel = Point2d((foundPixel.x + neighbours[position].x), (foundPixel.y + neighbours[position].y));
			//add the found pixel to the contour vector
			contourVec.push_back(foundPixel);
			//search the next neighbour pixel starting from the new found pixel
			//go back two positions so we don't miss any pixels going diagonally
			return findNeighbour(inputImage, position - 2, firstPxl, foundPixel, contourVec);
		}
		//if no neighbour found at current position search for the next one
		else {
			// increase position
			position++;
		}
	}
	// if for loop is ending all positions have been searched and the contour is not complete
	return false;
}

// func: measures the bending energy of a contour on a scale of 0 to 1
// pre : contourVec: contains the points of a contour
// return_value: the bending energy of the contour
double bendingEnergy(const vector<Point>& contourVec) {
	vector<int> normalizedChaincode = normalizeChaincode(getChaincode(contourVec));

	//create a double to store the bendingEnergy in.
	double bendingEnergy = 0;

	//calculate the total bending energy used:
	for (int i = 0; i < normalizedChaincode.size(); i++) {
		bendingEnergy = bendingEnergy + normalizedChaincode[i];
	}
	return bendingEnergy / 7 / normalizedChaincode.size();
}

// func: gets the chaincode out of a contourvec
// pre : chaincode: a chaincode has been determined
// return_value: a vector with the normalized chaincode.
vector<int> getChaincode(const vector<Point>& contourVec) {
	//we are using the following lookup table starting at the left going clockwise
	//-- 1 -- 2 -- 3 --
	//-- 0 -- x -- 4 --
	//-- 7 -- 6 -- 5 --
	//list of points that are possible deltas as described above we use point2d to store these
	vector<Point2d> deltas = { Point2d(-1,0), Point2d(-1,1), Point2d(0,1), Point2d(1,1), Point2d(1,0),Point2d(1,-1),Point2d(0,-1), Point2d(-1,-1) };

	//create the vector
	vector<int> chaincode;

	for (int i = 0; i < contourVec.size(); i++) {

		//make sure the next entry is the first one if last item has been reached
		int next = (i == contourVec.size() - 1) ? 0 : i + 1;

		int deltaX = contourVec[next].x - contourVec[i].x;
		int deltaY = contourVec[next].y - contourVec[i].y;

		for (int j = 0; j < 8; j++) {
			if (deltaX == deltas[j].x && deltaY == deltas[j].y) {
				chaincode.push_back(j);
				break;
			}
		}
	}
	return chaincode;
}

// func: normalizes a chaincode to make it rotation independent
// pre : chaincode: a chaincode has been determined
// return_value: a vector with the normalized chaincode.
vector<int> normalizeChaincode(vector<int>& chaincode) {
	//init the vector to return
	vector<int> normalizedChaincode;
	
	for (int i = 0; i < chaincode.size(); i++) {
		//make sure the next entry is the first one if last item has been reached
		int previous= (i == 0) ? chaincode.size() - 1 : i - 1;
		
		//determine the delta change
		//if no delta change
		if (chaincode[i] == chaincode[previous]) {
			normalizedChaincode.push_back(0);
		}
		//if delta change and current is bigger than previous
		else if (chaincode[i] > chaincode[previous]) {
			normalizedChaincode.push_back(chaincode[i]-chaincode[previous]);
		}
		//if delta change and previous is bigger than current
		else {
			normalizedChaincode.push_back(8 - chaincode[previous] + chaincode[i]);
		}
	}
	return normalizedChaincode;
}